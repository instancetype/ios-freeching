//
//  SDIMainViewController.m
//  Free Ching
//
//  Created by SDriversInn on 4/27/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import "SDIMainViewController.h"
#import "SDIHexagram.h"
#import "SDIChing.h"
#import "SDIHexViewController.h"

@interface SDIMainViewController ()

@property (nonatomic, strong) NSMutableArray *lines;
@property (nonatomic, strong) SDIHexagram *hexagram;
@property (weak, nonatomic) IBOutlet UILabel *hexLabel;
@property (weak, nonatomic) IBOutlet UILabel *hexDescriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIImageView *line1view;
@property (weak, nonatomic) IBOutlet UIImageView *line2view;
@property (weak, nonatomic) IBOutlet UIImageView *line3view;
@property (weak, nonatomic) IBOutlet UIImageView *line4view;
@property (weak, nonatomic) IBOutlet UIImageView *line5view;
@property (weak, nonatomic) IBOutlet UIImageView *line6view;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeRecognizer;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapRecognizer;

@end

@implementation SDIMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _lines = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.imageView.image = [UIImage imageNamed:@"ChingSpinner"]; //[UIImage imageNamed:@"5elements"];
    self.imageView.contentMode = UIViewContentModeScaleToFill;
    self.resetButton.hidden = YES;
    self.tapRecognizer.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)makeLine
{
    SDILine *line = [SDILine makeLine];
    [self.lines addObject:line];
    
    NSUInteger linesCount = [self.lines count];

    UIImage *image;
    if (line.quality == 0) {
        image = [UIImage imageNamed:@"yinline"];
    }
    else {
        image = [UIImage imageNamed:@"yangline"];
    }
    
    switch (linesCount) {
        case 1:
            self.line1view.image = image;
            break;
        case 2:
            self.line2view.image = image;
            break;
        case 3:
            self.line3view.image = image;
            break;
        case 4:
            self.line4view.image = image;
            break;
        case 5:
            self.line5view.image = image;
            break;
        case 6:
            self.line6view.image = image;
            break;
    }
    
    self.swipeRecognizer.enabled = YES;
    
    if (linesCount >= 6) {
        [self displayHexagram];
    }
}

- (void)displayHexagram
{
    self.swipeRecognizer.enabled = NO;
    self.tapRecognizer.enabled = YES;
    
    SDIChing *myChing = [SDIChing sharedChing];
    self.hexagram = [[SDIHexagram alloc] initWithLinesArray:self.lines];
    NSString *hexagramChineseName = [myChing chineseNameFor:self.hexagram];
    NSString *hexagramNumber = [myChing numberFor:self.hexagram];
    NSString *hexagramEnglishName = [myChing englishNameFor:self.hexagram];
    
    self.hexLabel.alpha = 0;
    self.hexLabel.text = hexagramChineseName;
    self.hexDescriptionLabel.alpha = 0;
    self.hexDescriptionLabel.text = [NSString stringWithFormat:@"%@. %@", hexagramNumber, hexagramEnglishName];
    
    
    [UIView animateWithDuration:1 animations:^{
        self.hexLabel.alpha = 1;
        self.hexDescriptionLabel.alpha = 1;
        self.imageView.alpha = 0;
    }];
}

- (IBAction)shuffle:(id)sender
{
     self.swipeRecognizer.enabled = NO;
    [self runSpinAnimationWithDuration:1];
}

- (void) runSpinAnimationWithDuration:(CGFloat) duration;
{
    CGFloat rotations = (arc4random() % 3) + 1;
    CGFloat time = (arc4random() % 2) + 1;
    
    [CATransaction begin];
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * rotations * time ];
    rotationAnimation.duration = time;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1.0;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    [CATransaction setCompletionBlock:^{
        [self makeLine];
    }];
    [self.imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [CATransaction commit];
    
}

- (IBAction)hexagramPageDisplay:(id)sender
{
    SDIChing *myChing = [SDIChing sharedChing];
    NSString *hexagramSummary = [myChing summaryFor:self.hexagram];
    
    SDIHexViewController *hvc = [[SDIHexViewController alloc] initWithSummary:hexagramSummary];
    hvc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    __weak SDIMainViewController *wself = self;
    [self presentViewController:hvc animated:YES completion:^{
        SDIMainViewController *sself = wself;
        sself.resetButton.hidden = NO;
        sself.resetButton.alpha = 1.0;
    }];
}

- (IBAction)reset:(id)sender
{
    [self.lines removeAllObjects];
    self.imageView.hidden = NO;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.resetButton.alpha = 0.0;
        self.imageView.alpha = 1.0;
        self.hexDescriptionLabel.alpha = 0.0;
        self.hexLabel.alpha = 0.0;
        self.line1view.alpha = 0.0;
        self.line2view.alpha = 0.0;
        self.line3view.alpha = 0.0;
        self.line4view.alpha = 0.0;
        self.line5view.alpha = 0.0;
        self.line6view.alpha = 0.0;
    }
    completion:^(BOOL finished){
        self.resetButton.hidden = YES;
        self.hexDescriptionLabel.text = @"";
        self.hexLabel.text = @"";
        self.hexDescriptionLabel.alpha = 1.0;
        self.hexLabel.alpha = 1.0;
        self.line1view.image = nil;
        self.line2view.image = nil;
        self.line3view.image = nil;
        self.line4view.image = nil;
        self.line5view.image = nil;
        self.line6view.image = nil;
        self.line1view.alpha = 1.0;
        self.line2view.alpha = 1.0;
        self.line3view.alpha = 1.0;
        self.line4view.alpha = 1.0;
        self.line5view.alpha = 1.0;
        self.line6view.alpha = 1.0;
        
        self.swipeRecognizer.enabled = YES;
        self.tapRecognizer.enabled = NO;
    }];
}
@end
