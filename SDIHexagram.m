//
//  SDIHexagram.m
//  Free Ching
//
//  Created by SDriversInn on 4/27/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//


/*  Note on binary interpretation of hexagrams: The BOTTOM line is given most significance as if the hexagram 
    is rotated 90 degrees clockwise. This is in accordance with the Fuxi sequence.
    See http://www.biroco.com/yijing/sequence.htm for further explanation.
*/


#import "SDIHexagram.h"

@implementation SDIHexagram

- (instancetype)initWithLine1:(SDILine *)l1 line2:(SDILine *)l2 line3:(SDILine *)l3 line4:(SDILine *)l4 line5:(SDILine *)l5 line6:(SDILine *)l6
{
    self = [super init];
    if (self) {
        _binaryValue = [NSNumber numberWithInt:
                        (int)((l1.quality << 5) +
                         (l2.quality << 4) +
                         (l3.quality << 3) +
                         (l4.quality << 2) +
                         (l5.quality << 1) +
                         l6.quality)];
        
        _line1 = l1;
        _line2 = l2;
        _line3 = l3;
        _line4 = l4;
        _line5 = l5;
        _line6 = l6;
    }
    return self;
}

- (instancetype)initWithLinesArray:(NSArray *)lines
{
    return [self initWithLine1:lines[0]
                         line2:lines[1]
                         line3:lines[2]
                         line4:lines[3]
                         line5:lines[4]
                         line6:lines[5]];
}

- (instancetype)init
{
    SDILine *line = [[SDILine alloc] init];
    
    return [self initWithLine1:line line2:line line3:line line4:line line5:line line6:line];
}

- (BOOL)isChanging
{
    BOOL changing = NO;
    
    if ([self.binaryValue  isEqual: @0] || [self.binaryValue  isEqual: @63]) {
        
        if (self.line1.moving && self.line2.moving && self.line3.moving &&
            self.line4.moving && self.line5.moving && self.line6.moving) {
            
            return changing;
        }
    }
    if (self.line1.moving || self.line2.moving || self.line3.moving ||
        self.line4.moving || self.line5.moving || self.line6.moving) {
        
        changing = YES;
    }
    return changing;
}
@end
