//
//  SDIAppDelegate.h
//  Free Ching
//
//  Created by SDriversInn on 4/27/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
