//
//  main.m
//  Free Ching
//
//  Created by SDriversInn on 4/27/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDIAppDelegate class]));
    }
}
