//
//  SDIChing.m
//  Free Ching
//
//  Created by SDriversInn on 4/28/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import "SDIChing.h"
#import "SDIHexagram.h"

@implementation SDIChing

+ (instancetype)sharedChing
{
    static SDIChing *sharedChing = nil;
    if (!sharedChing) {
        sharedChing = [[self alloc] initPrivate];
    }
    return sharedChing;
}

- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use +[SDIChing sharedChing]" userInfo:nil];
    
    return nil;
}

- (instancetype)initPrivate
{
    self = [super init];
    if (self) {
        _reference = @{@0: @{@"number": @2,
                             @"chineseName": @"Kun",
                             @"englishName": @"Earth",
                             @"summary": @"Let a major sacrifice be conducted. The omen of the mare is favorable.\n\nThe noble person"
                             @" proceeds with a purpose. You may at first be lost but later gain a master.\n\nA friend will be gained"
                             @" in the southwest; a friend will be lost in the northeast. This is for the best.\n\nThe omen of peace"
                             @" is auspicious."},
                       
                       @1: @{@"number": @23,
                             @"chineseName": @"Bo",
                             @"englishName": @"Faling Away",
                             @"summary": @"Splitting. It is not favorable to proceed with a purpose."},
                       
                       @2: @{@"number": @8,
                             @"chineseName": @"Bi",
                             @"englishName": @"Union",
                             @"summary": @"Standing together. Auspicious. The Primal Oracle: the fundamental and eternal omen.\n\n"
                             @"No harm.\n\nIn coming from a place of strife, the later man will meet with ill fate."},
                       
                       @3: @{@"number": @20,
                             @"chineseName": @"Guan",
                             @"englishName": @"Watching",
                             @"summary": @"The ablutions have been made, but there is no worship.\n\nMaintaining trust is a noble"
                             @" thing."},
                       
                       @4: @{@"number": @16,
                             @"chineseName": @"Yu",
                             @"englishName": @"Delight",
                             @"summary": @"Happiness. It is favorable to appoint feudal princes and to send out the army."},
                       
                       @5: @{@"number": @35,
                             @"chineseName": @"Jing",
                             @"englishName": @"Advancing",
                             @"summary": @"Marquis Kang presents the king with horses in abundance. Each day he is granted "
                             @"three audiences."},
                       
                       @6: @{@"number": @45,
                             @"chineseName": @"Cui",
                             @"englishName": @"Gathering",
                             @"summary": @"Let a sacrifice be conducted. When the king arrives, the temple has been "
                             @"established.\n\nIt is favorable to see the great person.\n\nLet a sacrifice be conducted. A "
                             @"favorable omen. Auspicious to make a great offering of cattle.\n\nIt is favorable to proceed "
                             @"with a purpose."},
                       
                       @7: @{@"number": @12,
                             @"chineseName": @"Pi",
                             @"englishName": @"Obstruction",
                             @"summary": @"Those who obstruct are bandits.\n\nThe omen of the noble person is unfavorable.\n\n"
                             @"Great things go; small things come."},
                       
                       @8: @{@"number": @15,
                             @"chineseName": @"Qian",
                             @"englishName": @"Modesty",
                             @"summary": @"Let a sacrifice be conducted. The noble person completes what he undertakes."},
                       
                       @9: @{@"number": @52,
                             @"chineseName": @"Gen",
                             @"englishName": @"Keeping Still",
                             @"summary": @"Your back is motionless. You will not be captured.\n\nWalking in your courtyard. "
                             @"You are not seen. No harm."},
                       
                       @10: @{@"number": @39,
                              @"chineseName": @"Jian",
                              @"englishName": @"Hardship",
                              @"summary": @"Impediments. Favorable in the southwest. Unfavorable in the northeast."},
                       
                       @11: @{@"number": @53,
                              @"chineseName": @"Jian",
                              @"englishName": @"Developing Gradually",
                              @"summary": @"Auspicious for the woman to marry. A favorable omen."},
                       
                       @12: @{@"number": @62,
                              @"chineseName": @"Xiao Guo",
                              @"englishName": @"Small Excess",
                              @"summary": @"Let a sacrifice be conducted. A favorable omen.\n\nYou can engage in small affairs, but"
                              @" do not engage in great affairs.\n\nThe sound left behind by a flying bird. It is not fitting to go"
                              @" high. It is fitting to stay low."},
                       
                       @13: @{@"number": @56,
                              @"chineseName": @"Lu",
                              @"englishName": @"The Traveler",
                              @"summary": @"Let a minor sacrifice be conducted.\n\nThe omen of the traveler is auspicious."},
                       
                       @14: @{@"number": @31,
                              @"chineseName": @"Xian",
                              @"englishName": @"Unity",
                              @"summary": @"Mutual influence. Let a sacrifice be conducted. A favorable omen. Auspicious to "
                              @"choose a wife"},
                       
                       @15: @{@"number": @33,
                              @"chineseName": @"Dun",
                              @"englishName": @"Retreat",
                              @"summary": @"Let a sacrifice be conducted. A favorable omen for small matters."},
                       
                       @16: @{@"number": @7,
                              @"chineseName": @"Shi",
                              @"englishName": @"The Army",
                              @"summary": @"The omen of the powerful man is auspicious. No harm."},
                       
                       @17: @{@"number": @4,
                              @"chineseName": @"Meng",
                              @"englishName": @"Ignorance",
                              @"summary": @"Let a sacrifice be conducted.\n\nIt was not I who sought the ignorant child; it was the "
                              @"ignorant child who sought me.\n\nI answered upon the initial asking. To ask two or three times "
                              @"shows disrespect.\n\nWhen there is disrespect, I do not reveal a favorable omen."},
                       
                       @18: @{@"number": @29,
                              @"chineseName": @"Kan",
                              @"englishName": @"Watery Pits",
                              @"summary": @"There is trust safeguarding the heart. Let a sacrifice be conducted.\n\nGoing forth is "
                              @"beneficial"},
                       
                       @19: @{@"number": @59,
                              @"chineseName": @"Huan",
                              @"englishName": @"Flowing",
                              @"summary": @"Let a sacrifice be conducted. When the king arrives, the temple has been "
                              @"established.\n\nIt is favorable to cross the great river. A favorable omen."},
                       
                       @20: @{@"number": @40,
                              @"chineseName": @"Jie",
                              @"englishName": @"Release",
                              @"summary": @"Favorable in the southwest.\n\nNowhere to go. Auspicious to return.\n\nIn "
                              @"proceeding with a purpose, it is auspicious to be early."},
                       
                       @21: @{@"number": @64,
                              @"chineseName": @"Wei Ji",
                              @"englishName": @"Not Yet Fulfilled",
                              @"summary": @"Before crossing the river. Let a sacrifice be conducted.\n\nWhen the small fox has "
                              @"nearly crossed the river, it gets its tail wet. There is no endeavor that will end favorably."},
                       
                       @22: @{@"number": @47,
                              @"chineseName": @"Kun",
                              @"englishName": @"Oppression",
                              @"summary": @"Let a sacrifice be conducted. The omen is auspicious for the great person. No harm.\n\n"
                              @"Do not believe the words that are spoken."},
                       
                       @23: @{@"number": @6,
                              @"chineseName": @"Song",
                              @"englishName": @"A Dispute",
                              @"summary": @"There is trust, yet there are obstructions. Be vigilant.\n\nMidway all things are "
                              @"auspicious, but the end will bring ill fate.\n\nIt is favorable to see the great person.\n\nIt is not "
                              @"favorable to cross the great river."},
                       
                       @24: @{@"number": @46,
                              @"chineseName": @"Sheng",
                              @"englishName": @"Ascending",
                              @"summary": @"Let a major sacrifice be conducted. See the great person. Do not be concerned.\n\n"
                              @"Auspicious for the army to advance to the south."},
                       
                       @25: @{@"number": @18,
                              @"chineseName": @"Gu",
                              @"englishName": @"Remedying",
                              @"summary": @"Corruption. Let a major sacrifice be conducted. It is favorable to cross the great "
                              @"river.\n\nWearing armor three days before, wearing armor three days after."},
                       
                       @26: @{@"number": @48,
                              @"chineseName": @"Jing",
                              @"englishName": @"The Well",
                              @"summary": @"Replenishing. Change the city, but do not change the well. No losses, no gains.\n\n"
                              @"Comings and goings at the well. The well is nearly dry.\n\nNor is a rope to be found at the well."
                              @" Your jug is fragile. Inauspicious."},
                       
                       @27: @{@"number": @57,
                              @"chineseName": @"Xun",
                              @"englishName": @"Cowardice",
                              @"summary": @"Let a minor sacrifice be conducted. It is favorable to proceed with a purpose.\n\n"
                              @"It is favorable to see the great person."},
                       
                       @28: @{@"number": @32,
                              @"chineseName": @"Heng",
                              @"englishName": @"Constancy",
                              @"summary": @"Long-lasting. Let a sacrifice be conducted. No harm. A favorable omen.\n\nIt is "
                              @"favorable to proceed with a purpose."},
                       
                       @29: @{@"number": @50,
                              @"chineseName": @"Ding",
                              @"englishName": @"The Tripod",
                              @"summary": @"Establishing the new. A major auspicious omen.\n\nLet a sacrifice be conducted."},
                       
                       @30: @{@"number": @28,
                              @"chineseName": @"Da Guo",
                              @"englishName": @"Great Excess",
                              @"summary": @"The ridgepole is twisted. It is favorable to proceed with a purpose.\n\nLet a sacrifice"
                              @" be conducted."},
                       
                       @31: @{@"number": @44,
                              @"chineseName": @"Gou",
                              @"englishName": @"An Encounter",
                              @"summary": @"The woman is strong. Do not choose the woman.\n\nTied to a golden block. The omen"
                              @" is auspicious.\n\nProceeding with purpose, you will see ill fate. One can trust even a "
                              @"weak pig to pace about."},
                       
                       @32: @{@"number": @24,
                              @"chineseName": @"Fu",
                              @"englishName": @"Returning",
                              @"summary": @"Let a sacrifice be conducted. Going and coming, there will be no affliction. Friends "
                              @"arrive. No harm.\n\nReversing and returning on the road. You return after seven days.\n\nIt is "
                              @"favorable to proceed with a purpose."},
                       
                       @33: @{@"number": @27,
                              @"chineseName": @"Yi",
                              @"englishName": @"Nourishing",
                              @"summary": @"Cheeks. The omen is auspicious. Look at your cheeks. Seek sustenance for yourself."},
                       
                       @34: @{@"number": @3,
                              @"chineseName": @"Zhun",
                              @"englishName": @"Difficulty",
                              @"summary": @"Let a major sacrifice be conducted. A favorable omen.\n\nDo not proceed with a purpose."
                              @" It is favorable to appoint feudal princes."},
                       
                       @35: @{@"number": @42,
                              @"chineseName": @"Yi",
                              @"englishName": @"Increase",
                              @"summary": @"It is favorable to proceed with a purpose.\n\nIt is favorable to cross the "
                              @"great river."},
                       
                       @36: @{@"number": @51,
                              @"chineseName": @"Zhen",
                              @"englishName": @"Thunder",
                              @"summary": @"Let a sacrifice be conducted.\n\nThe thunder comes, instilling dread, "
                              @"yet one laughs and talks.\n\nThe thunderclap is heard a hundred miles, yet not one ladle of "
                              @"sacrificial wine is spilled."},
                       
                       @37: @{@"number": @21,
                              @"chineseName": @"Shi He",
                              @"englishName": @"Eradicating",
                              @"summary": @"Biting and snapping. Let a major sacrifice be conducted.\n\nIt is favorable to invoke "
                              @"the law."},
                       
                       @38: @{@"number": @17,
                              @"chineseName": @"Sui",
                              @"englishName": @"Following",
                              @"summary": @"Let a major sacrifice be made. A favorable omen. No harm."},
                       
                       @39: @{@"number": @25,
                              @"chineseName": @"Wu Wang",
                              @"englishName": @"The Unexpected",
                              @"summary": @"Let a major sacrifice be conducted. A favorable omen.\n\nIn acting without honesty, you "
                              @"will be in error. It is not favorable to proceed with a purpose."},
                       
                       @40: @{@"number": @36,
                              @"chineseName": @"Ming Yi",
                              @"englishName": @"The Bright Pheasant",
                              @"summary": @"The omen of difficulty is favorable."},
                       
                       @41: @{@"number": @22,
                              @"chineseName": @"Bi",
                              @"englishName": @"Adornment",
                              @"summary": @"Let a sacrifice be made. In small matters, it is favorable to proceed with a purpose."},
                       
                       @42: @{@"number": @63,
                              @"chineseName": @"Ji Ji",
                              @"englishName": @"Already Fulfilled",
                              @"summary": @"After crossing the river. Let a sacrifice be conducted. A favorable omen for small "
                              @"affairs.\n\nAuspicious at first, but the end brings disarray."},
                       
                       @43: @{@"number": @37,
                              @"chineseName": @"Jia Ren",
                              @"englishName": @"Household",
                              @"summary": @"Family. The omen of the woman is favorable."},
                       
                       @44: @{@"number": @55,
                              @"chineseName": @"Feng",
                              @"englishName": @"Abundance",
                              @"summary": @"Let a sacrifice be conducted. The king arrives. Do not be troubled. It is fitting to "
                              @"act at Midday."},
                       
                       @45: @{@"number": @30,
                              @"chineseName": @"Li",
                              @"englishName": @"Fire",
                              @"summary": @"A favorable omen. Let a sacrifice be conducted.\n\nAuspicious to raise a cow."},
                       
                       @46: @{@"number": @49,
                              @"chineseName": @"Ge",
                              @"englishName": @"Revolt",
                              @"summary": @"Abolishing the old. On the Sixth day there will be trust. Let a major sacrifice "
                              @"be conducted.\n\nA favorable omen. Regrets will disappear."},
                       
                       @47: @{@"number": @13,
                              @"chineseName": @"Tong Ren",
                              @"englishName": @"Gathering",
                              @"summary": @"Seeking harmony. People gather in the field. Let a sacrifice be conducted.\n\nIt is"
                              @" favorable to cross the great river. The omen of the noble person is favorable."},
                       
                       @48: @{@"number": @19,
                              @"chineseName": @"Lin",
                              @"englishName": @"Approaching",
                              @"summary": @"Let a major sacrifice be conducted. A favorable omen.\n\nThe arrival of the eighth month"
                              @" brings ill fate."},
                       
                       @49: @{@"number": @41,
                              @"chineseName": @"Sun",
                              @"englishName": @"Decrease",
                              @"summary": @"There is trust. A major auspicious omen.\n\nNo harm. You can engage in "
                              @"divination. It is favorable to proceed with a purpose.\n\nHow should one proceed? Using two "
                              @"platters, you can conduct the sacrifice."},
                       
                       @50: @{@"number": @60,
                              @"chineseName": @"Jie",
                              @"englishName": @"Restraint",
                              @"summary": @"Let a sacrifice be conducted. Bitter restraint. Do not engage in divination."},
                       
                       @51: @{@"number": @61,
                              @"chineseName": @"Zhong Fu",
                              @"englishName": @"Innermost Sincerity",
                              @"summary": @"Swine and fish are auspicious.\n\nIt is favorable to cross the great river. A "
                              @"favorable omen."},
                       
                       @52: @{@"number": @54,
                              @"chineseName": @"Giu Mei",
                              @"englishName": @"The Marrying Maiden",
                              @"summary": @"Inauspicious for the army to advance. There is no endeavor that will end favorably."},
                       
                       @53: @{@"number": @38,
                              @"chineseName": @"Kui",
                              @"englishName": @"Diversity",
                              @"summary": @"Auspicious for small affairs."},
                       
                       @54: @{@"number": @58,
                              @"chineseName": @"Dui",
                              @"englishName": @"Joy",
                              @"summary": @"Let a sacrifice be conducted. A favorable omen."},
                       
                       @55: @{@"number": @10,
                              @"chineseName": @"Lu",
                              @"englishName": @"Treading",
                              @"summary": @"You are treading on the tiger's tail, but the tiger does not bite.\n\nLet a sacrifice "
                              @"be conducted."},
                       
                       @56: @{@"number": @11,
                              @"chineseName": @"Tai",
                              @"englishName": @"Advance",
                              @"summary": @"Peace. Small things go; great things come. Auspicious.\n\nLet a sacrifice be "
                              @"conducted."},
                       
                       @57: @{@"number": @26,
                              @"chineseName": @"Da Xu",
                              @"englishName": @"Great Accumulation",
                              @"summary": @"Big cattle. A favorable omen. Auspicious to dine away from home.\nIt is favorable to "
                              @"cross the great river."},
                       
                       @58: @{@"number": @5,
                              @"chineseName": @"Xu",
                              @"englishName": @"Waiting",
                              @"summary": @"Trust and glory. Let a sacrifice be conducted.\n\nThe omen is auspicious. It is favorable"
                              @" to cross the great river."},
                       
                       @59: @{@"number": @9,
                              @"chineseName": @"Xiao Xu",
                              @"englishName": @"Little Accumulation",
                              @"summary": @"Small cattle. Let a sacrifice be conducted.\n\nThick clouds, without rain, approaching "
                              @"from my western fields."},
                       
                       @60: @{@"number": @34,
                              @"chineseName": @"Da Zuang",
                              @"englishName": @"Great Strength",
                              @"summary": @"A favorable omen."},
                       
                       @61: @{@"number": @14,
                              @"chineseName": @"Da You",
                              @"englishName": @"Great Harvest",
                              @"summary": @"Great possession. Let a major sacrifice be conducted."},
                       
                       @62: @{@"number": @43,
                              @"chineseName": @"Guai",
                              @"englishName": @"Striding",
                              @"summary": @"An announcement is made at the court of the king. A cry of solidarity.\n\n"
                              @"Danger. Reports come in from the cities. It is not favorable for the army to advance.\n\n"
                              @"It is favorable to proceed with a purpose."},
                       
                       @63: @{@"number": @1,
                              @"chineseName": @"Qian",
                              @"englishName": @"Heaven",
                              @"summary": @"Let a major sacrifice be conducted. A favorable omen."}};
    }
    return self;
}

- (NSString *)summaryFor:(SDIHexagram *)hexagram
{
    NSDictionary *hexInfo = self.reference[hexagram.binaryValue];
    
    return hexInfo[@"summary"];
}

- (NSString *)numberFor:(SDIHexagram *)hexagram
{
    NSDictionary *hexInfo = self.reference[hexagram.binaryValue];
    
    return hexInfo[@"number"];
}

- (NSString *)englishNameFor:(SDIHexagram *)hexagram
{
    NSDictionary *hexInfo = self.reference[hexagram.binaryValue];
    
    return hexInfo[@"englishName"];
}

- (NSString *)chineseNameFor:(SDIHexagram *)hexagram
{
    NSDictionary *hexInfo = self.reference[hexagram.binaryValue];
    
    return hexInfo[@"chineseName"];
}

@end
    
