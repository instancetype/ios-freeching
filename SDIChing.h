//
//  SDIChing.h
//  Free Ching
//
//  Created by SDriversInn on 4/28/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SDIHexagram;

@interface SDIChing : NSObject
@property (nonatomic, copy) NSDictionary *reference;

+ (instancetype)sharedChing;

- (NSString *)summaryFor:(SDIHexagram *)hexagram;
- (NSString *)numberFor:(SDIHexagram *)hexagram;
- (NSString *)englishNameFor:(SDIHexagram *)hexagram;
- (NSString *)chineseNameFor:(SDIHexagram *)hexagram;

@end
