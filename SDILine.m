//
//  SDILine.m
//  Free Ching
//
//  Created by SDriversInn on 4/27/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import "SDILine.h"

@implementation SDILine

+ (SDILine *)makeLine
{
    int roll = (arc4random() % 4) + 6;
    SDILineQuality quality;
    BOOL moving;
    
    switch (roll) {
        case 6:
            quality = yin;
            moving = YES;
            break;
            
        case 7:
            quality = yang;
            moving = NO;
            break;
            
        case 8:
            quality = yin;
            moving = NO;
            break;
            
        case 9:
            quality = yang;
            moving = YES;
            break;
            
        default:
            quality = yin;
            moving = NO;
    }
    return [[SDILine alloc] initWithQuality:quality andMoving:moving];
}

- (instancetype)initWithQuality:(SDILineQuality)quality andMoving:(BOOL)moving
{
    self = [super init];
    if (self) {
        _quality = quality;
        _moving = moving;
    }
    
    return self;
}

- (instancetype)init
{
    return [self initWithQuality:yin andMoving:NO];
}


@end
