//
//  SDIHexagram.h
//  Free Ching
//
//  Created by SDriversInn on 4/27/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDILine.h"

@interface SDIHexagram : NSObject

@property (nonatomic, strong) NSNumber *binaryValue;
@property (nonatomic, strong) SDILine *line1, *line2, *line3, *line4, *line5, *line6;

- (instancetype)initWithLine1:(SDILine *)l1
                        line2:(SDILine *)l2
                        line3:(SDILine *)l3
                        line4:(SDILine *)l4
                        line5:(SDILine *)l5
                        line6:(SDILine *)l6;

- (instancetype)initWithLinesArray:(NSArray *)lines;
- (BOOL)isChanging;

@end
