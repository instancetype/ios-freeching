//
//  SDIHexViewController.m
//  Free Ching
//
//  Created by SDriversInn on 4/30/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import "SDIHexViewController.h"
#import "SDIMainViewController.h"

@interface SDIHexViewController ()

@end

@implementation SDIHexViewController

- (instancetype)initWithSummary:(NSString *)summary
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _summary = summary;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.textView.editable = NO;
    self.textView.textContainerInset = UIEdgeInsetsMake(8, 24, 8, 24);
    self.textView.text = self.summary;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissView:(id)sender
{

    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
    }];
}


@end
