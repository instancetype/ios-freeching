//
//  SDILine.h
//  Free Ching
//
//  Created by SDriversInn on 4/27/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//



#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SDILineQuality) {
    yin,
    yang
};

@interface SDILine : NSObject

@property SDILineQuality quality;
@property BOOL moving;

+ (SDILine *)makeLine;
- (instancetype)initWithQuality:(SDILineQuality)quality andMoving:(BOOL)moving;

@end
