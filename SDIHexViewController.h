//
//  SDIHexViewController.h
//  Free Ching
//
//  Created by SDriversInn on 4/30/14.
//  Copyright (c) 2014 SDriversInn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDIHexViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) NSString *summary;

- (instancetype)initWithSummary:(NSString *)summary;

@end
